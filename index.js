// TODO: multiple customers support (grab from file)


const path = require("path");

require("dotenv/config");

const choices = {
    login: process.env.APP_USER_EMAIL,
    password: process.env.APP_USER_PASSWORD,
    centre: process.env.APP_VISA_CENTER,
    appointmentCategory: process.env.APP_VISA_CATEGORY,

    passportNumber: process.env.APP_PASSPORT_NUMBER,
    birthDate: process.env.APP_BIRTH_DATE,
    expiryDate: process.env.APP_PASSPORT_EXPIRE_DATE,
    nationality: process.env.APP_NATIONALITY,
    firstName: process.env.APP_FIRST_NAME,
    lastName: process.env.APP_LAST_NAME,
    gender: process.env.APP_GENDER,
    mobileNumber: process.env.APP_PHONE,
    email: process.env.APP_CUSTOMER_EMAIL || process.env.APP_USER_EMAIL,
};
const internal = {
    targetLanguage: 'english',
    zeroPageTitle: 'Appointment Login',
    loggedInText: 'Apply for visa',
    loggedInResidentText: 'Apply for Resident Permit',
    addCustomerText: 'Add Customer',

    zeroPageTitleSelector: 'h1',
    loggedInSelector: '#header-title',
    languageSelector: '#LanguageId',
    emailSelector: '#EmailId',
    passwordSelector: '#Password',
    authButtonSelector: '#btnSubmit',
    leftPanelSelector: '.leftpanel',
    rightPanelSelector: '.rightpanel',
    centreSelector: '#LocationId',
    appointmentCategorySelector: '#VisaCategoryId',
    loaderSelector: '#loading',
    continueButtonSelector: '#btnContinue',
    addCustomerSelector: 'a.submitbtn',
    customerPassportNumberSelector: '#PassportNumber',
    customerBirthDateSelector: '#DateOfBirth',
    customerPassportExpireDateSelector: '#PassportExpiryDate',
    customerNationalitySelector: '#NationalityId',
    customerFirstNameSelector: '#FirstName',
    customerLastNameSelector: '#LastName',
    customerGenderSelector: '#GenderId',
    customerPhoneNumberSelector: '#Mobile',
    customerEmailSelector: '#validateEmailId',
    customerSubmitSelector: '#submitbuttonId',
    referenceNumberSelector: '.rightpanel .mandatory-txt label b',
    customerListSubmitSelector: 'input.submitbtn[type="submit"]',
    dateConfirmSelector: '#btnConfirm',
    dateCalendarSelector: '#calendar',
    dateMonthSelector: '#calendar h2',
    dateDaySelector: '#calendar .fc-day.fc-future',
    dateTimeSelector: '#TimeBandsDiv input[type="radio"]',
    reachVFSSelector: '#ReachVFS',
};
const refreshInterval = (Math.max(Number(process.env.APP_REFRESH_INTERVAL) || 1, 1)) * 60 * 1000; // in ms
const soundFilePath = process.env.APP_SOUND_FILEPATH || './alarm-1.ogg';
const enableSoundAttention = process.env.APP_ENABLE_SOUND_ATTENTION !== undefined ? !!process.env.APP_ENABLE_SOUND_ATTENTION : true;

const log_original = require("./logger")(document);
function log(...args) {
    log_original(new Date().toLocaleString('ru'), ...args);
}

const {
  sleep,
  openURLAndGetNWWindow,
  prepareGoogleCaptchaAvoidness,
  getReCaptchaIFrame,
  waitForPageUnload,
  waitForChromiumEvent,
} = require("./utils");

// const { getEmails, markAsRead, sendEmail } = require('./email');

const url = "https://www.vfsvisaservicesrussia.com/Global-Appointment/Account/RegisteredLogin?q=shSA0YnE4pLF9Xzwon/x/FXkgptUe6eKckueax3hilyMCJeF9rpsVy6pNcXQaW1lbU6dflaYjPdspHRzryoe8Q==";

const state = {
    enabled: true,
    restartRequired: false,
};

async function scheduleRestart(window) {
    state.restartRequired = true;
}

async function waitForLoadingEnds(window) {
    log('wait while loading ends');
    const loader = window.document.querySelector(internal.loaderSelector);
    if (loader?.style.display === 'none' || loader?.style.display === '') {
        await sleep(500);
    }
    while (window.document.querySelector(internal.loaderSelector)?.style.display !== 'none') {
        await sleep(100);
    }
    log('loading done');
}

let lastAuthorization = 0;
let googleCaptchaCounter = 0;
let hcaptchaCounter = 0;

/** @type {(window: Window, nwwindow: NWJS_Helpers.win) => Promise<void>} */
async function step0_changeLanguage(window, nwwindow) {
    log('changing language to:', internal.targetLanguage);
    /** @type {HTMLSelectElement} */
    let language = window.document.querySelector(internal.languageSelector);
    // inject our own confirm function to not to block UI
    window.confirm = () => true;
    // grab jquery to trigger the event
    const jq = window.$;
    for (const option of language.options) {
        if (option.innerText.toLowerCase()
            .includes(internal.targetLanguage.toLowerCase())
        ) {
            option.selected = true;
        } else {
            option.selected = false;
        }
    }
    jq(language).trigger('change');
    await waitForChromiumEvent(nwwindow, 'loaded');
}

/** @type {(window: Window, nwwindow: NWJS_Helpers.win) => Promise<void>} */
async function step0_authorize(window, nwwindow) {
    log('authorize as:', choices.login);
    if (Date.now() - 2 * refreshInterval < lastAuthorization) {
        // prevent multiple authorizations in ~2 minutes ban
        const sleepTime = lastAuthorization - (Date.now() - 2 * refreshInterval);
        log('wait', Math.round(sleepTime * 1e-3), 's');
        await sleep(sleepTime);
    }
    const email = window.document.querySelector(internal.emailSelector);
    const password = window.document.querySelector(internal.passwordSelector);
    const button = window.document.querySelector(internal.authButtonSelector);
    const captcha = getReCaptchaIFrame(window);
    email.value = choices.login;
    password.value = choices.password;
    if (!captcha) {
        log('No captcha window found');
        window.location.href = url;
        await waitForChromiumEvent(nwwindow, 'loaded');
        return;
    }
    await sleep(50);
    captcha.contentWindow.document.querySelector('.recaptcha-checkbox-border').click();
    // check, if captcha is OK
    let status = false;
    const start = Date.now();
    const el = captcha.contentWindow.document.querySelector('.recaptcha-checkbox-checkmark').parentElement;
    while (!status) {
        await sleep(100);
        if (el.classList.contains('recaptcha-checkbox-checked')) {
            status = true;
        }
        if (Date.now() - start > 2 * refreshInterval) {
            googleCaptchaCounter++;
            if (googleCaptchaCounter > 4) {
                state.restartRequired = true;
                googleCaptchaCounter = 0;
            } else {
                window.location.reload();
                await waitForChromiumEvent(nwwindow, 'loaded');
            }
            return;
        }
    }
    googleCaptchaCounter = 0;
    button.click();
    await waitForChromiumEvent(nwwindow, 'loaded');
    lastAuthorization = Date.now();
    log('authorize done');
}

/** @type {(window: Window, nwwindow: NWJS_Helpers.win) => Promise<void>} */
async function step0_moveToForm(window, nwwindow) {
    log('go to form');
    /** @type {NodeListOf<HTMLAnchorElement>} */
    const links = window.document.querySelectorAll('a');
    for (const link of links) {
        if (link.href.includes('/SelectVAC')) {
            link.click();
            break;
        }
    }
    await waitForChromiumEvent(nwwindow, 'loaded');
}

/** @type {(window: Window, nwwindow: NWJS_Helpers.win) => Promise<void>} */
async function step1(window) {
    log('step1: select centre');
    /** @type {HTMLSelectElement} */
    const centre = window.document.querySelector(internal.centreSelector);
    const jq = window.$;
    for (const option of centre.options) {
        if (option.innerText.toLowerCase().includes(choices.centre.toLowerCase())) {
            option.selected = true;
            break;
        }
    }
    await sleep(10);
    jq(centre).trigger('change');
    await sleep(500);
    await Promise.race([ waitForLoadingEnds(window), sleep(5000) ]);
}

/** @type {(window: Window, nwwindow: NWJS_Helpers.win) => Promise<void>} */
async function step2(window, nwwindow) {
    log('step2: select category');
    await sleep(100);
    /** @type {HTMLSelectElement} */
    const centre = window.document.querySelector(internal.centreSelector);
    /** @type {HTMLSelectElement} */
    const category = window.document.querySelector(internal.appointmentCategorySelector);
    if (category.disabled || !centre.selectedIndex) {
        log('step2: categories disabled (no seats)');
        await sleep(refreshInterval);
        window.location.reload();
        await waitForChromiumEvent(nwwindow, 'loaded');
        return;
    }
    log('step2: categories enabled');
    const jq = window.$;
    for (const option of category.options) {
        if (option.innerText.toLowerCase().includes(choices.appointmentCategory.toLowerCase())) {
            option.selected = true;
            break;
        }
    }
    // await sleep(100);
    // jq(category).trigger('change');
    // await sleep(250);
    // await Promise.race([ waitForLoadingEnds(window), sleep(5000) ]);
    // console.log('step2: loading ends');
    // await sleep(100);
    if (!centre.selectedIndex || !category.selectedIndex || category.disabled) {
        // false-positive
        window.location.reload();
        await waitForChromiumEvent(nwwindow, 'loaded');
        return;
    }
    // await sleep();
    const nextBtn = window.document.querySelector(internal.continueButtonSelector);
    console.log('step2: click');
    if (nextBtn.disabled) {
        nextBtn.disabled = false;
    }
    nextBtn.click();
    await Promise.race([ waitForChromiumEvent(nwwindow, 'loaded'), sleep(5000) ]);
    console.log('step2: loaded');
}

/** @type {(window: Window, nwwindow: NWJS_Helpers.win) => Promise<void>} */
async function step3(window, nwwindow) {
    // page: empty appointment
    console.log('step3');
    // 'https://www.vfsvisaservicesrussia.com/Global-Appointment/Applicant/ApplicantList'
    /** @param {Window} window */
    function getAddCustomerButton(window) {
        // a.submitbtn with "Add Customer" text
        let buttons = Array.from(window.document.querySelectorAll(internal.addCustomerSelector));
        for (const idx in buttons) {
            const button = buttons[idx];
            if (!button.textContent.toLowerCase().includes(internal.addCustomerText.toLowerCase())) {
                buttons.splice(idx, 1);
            }
        }

        if (buttons.length > 1) {
            log('more than 1 Add customer buttons found');
            state.enabled = false;
            return null;
        } else if (buttons.length === 0) {
            log('no Add customer buttons found');
            state.restartRequired = true;
            return null;
        }
        
        return buttons[0];
    }

    let addCustomer = getAddCustomerButton(window);
    console.log(addCustomer);
    if (!addCustomer) return;

    addCustomer.click();
    await waitForChromiumEvent(nwwindow, 'loaded');

    // next page: customer fill

    window = nwwindow.window;
    await sleep(100);
    window.confirm = () => true;
    log('filling the form');
    const passportNumber = window.document.querySelector(internal.customerPassportNumberSelector);
    const birthDate = window.document.querySelector(internal.customerBirthDateSelector);
    const expireDate = window.document.querySelector(internal.customerPassportExpireDateSelector);
    /** @type {HTMLSelectElement} */
    const nationality = window.document.querySelector(internal.customerNationalitySelector);
    const firstName = window.document.querySelector(internal.customerFirstNameSelector);
    const lastName = window.document.querySelector(internal.customerLastNameSelector);
    const gender = window.document.querySelector(internal.customerGenderSelector);
    const phone = window.document.querySelector(internal.customerPhoneNumberSelector);
    const email = window.document.querySelector(internal.customerEmailSelector);
    const submitButton = window.document.querySelector(internal.customerSubmitSelector);

    passportNumber.value = choices.passportNumber;
    birthDate.value = choices.birthDate;
    expireDate.value = choices.expiryDate;
    for (const option of nationality.options) {
        if (option.textContent.toLowerCase().includes(choices.nationality.toLowerCase())) {
            option.selected = true;
            break;
        } else {
            option.selected = false;
        }
    }
    firstName.value = choices.firstName;
    lastName.value = choices.lastName;
    for (const option of gender.options) {
        if (option.textContent.toLowerCase() == choices.gender.toLowerCase()) {
            option.selected = true;
            break;
        } else {
            option.selected = false;
        }
    }
    phone.value = choices.mobileNumber;
    email.value = choices.email;
    submitButton.click();
    log('form filled and submitted');
    await waitForChromiumEvent(nwwindow, 'loaded');
    
    // next page: after customer fill

    window = nwwindow.window;
    const referenceNumber = window.document.querySelector(internal.referenceNumberSelector);
    const submitButton2 = window.document.querySelector(internal.customerListSubmitSelector);
    if (!referenceNumber) {
        window.location.reload();
        await waitForChromiumEvent(nwwindow, 'loaded');
        return
    }
    log('reference number: ', referenceNumber.textContent);
    submitButton2.click();
    await waitForChromiumEvent(nwwindow, 'loaded');

    // next page: date-time selection

    window = nwwindow.window;
    window.confirm = () => true;
    let jq = window.$;
    /** @type {string} "July 2022" */
    const month = window.document.querySelector(internal.dateMonthSelector).textContent;
    const days = Array.from(window.document.querySelectorAll(internal.dateDaySelector));
    /** @type {HTMLTableCellElement} */
    let availableDayElement;
    for (const day of days) {
        if (day.style.cursor === 'pointer') {
            availableDayElement = day;
            break;
        }
    }
    if (!availableDayElement) {
        log('calendar without available dates');
        window.location.reload();
        await waitForChromiumEvent(nwwindow, 'loaded');
        return;
    }
    log(`calendar has date: ${availableDayElement.dataset.date}`);
    // do NOT remove space below! negates auto-UTC time
    const date = new Date(availableDayElement.dataset.date + ' ');
    const calendarElement = window.document.querySelector(internal.dateCalendarSelector);
    if (!calendarElement) {
        log('no calendar found');
        window.location.reload();
        await waitForChromiumEvent(nwwindow, 'loaded');
        return;
    }
    const calendarInstance = jq.data(calendarElement, 'fullCalendar');
    if (!calendarInstance) {
        log('no calendar jq instance found');
        window.location.reload();
        await waitForChromiumEvent(nwwindow, 'loaded');
        return;
    }
    calendarInstance.options.dayClick(
        date,
        null,
        null,
        calendarInstance.getView()
    );
    await sleep(100);
    const timeElements = window.document.querySelectorAll(internal.dateTimeSelector);
    log(`day has ${timeElements.length} time entries`);
    if (timeElements.length === 0) {
        window.location.reload();
        await waitForChromiumEvent(nwwindow, 'loaded');
        return;
    }
    /** @type {HTMLInputElement} */
    const timeElement = timeElements[0];
    const time = timeElement.parentElement.nextElementSibling.textContent;
    log(`trying time: ${time}`);
    timeElement.checked = true;
    // possibly needed for VFS frontend validation
    timeElement.click();
    await sleep(100);
    const submitButton3 = window.document.querySelector(internal.dateConfirmSelector);
    submitButton3.click();
    await waitForChromiumEvent(nwwindow, 'loaded');
    await sleep(100);
    
    // final page
    
    window = nwwindow.window;
    state.enabled = false;
    const reachVFS = window.document.querySelector(internal.reachVFSSelector);
    if (reachVFS) {
        log('SUCCESS!');
    } else {
        log('FAILURE!');
    }
}

/** @type {(window: Window, nwwindow: NWJS_Helpers.win) => Promise<void>} */
async function hcaptcha(window, nwwindow) {
    log('cloudflare captcha found');
    if (
        window.document.body.innerText.includes('We are checking your browser')
        // || window.document.body.innerText.includes('www.vfsvisaservicesrussia.com')
        || window.document.title.toLowerCase().includes('cloudflare')
    ) {
        await Promise.race([
            waitForChromiumEvent(nwwindow, 'loaded'),
            sleep(60000)
        ]);
        hcaptchaCounter++;
        if (hcaptchaCounter > 4) {
            state.restartRequired = true;
            hcaptchaCounter = 0;
        }
    }
}

/** @type {(window: Window, nwwindow: NWJS_Helpers.win) => Promise<void>} */
async function serviceUnavailable(window, nwwindow) {
    log('service is unavailable, wait', Math.round(refreshInterval * 1e-3), 's');
    await sleep(refreshInterval);
    window.location.reload();
    await waitForChromiumEvent(nwwindow, 'loaded');
}





/** @type {Array<(window: Window) => (window: Window, nwwindow: NWJS_Helpers.win) => Promise<void> | undefined>} */
const checkers = [
    function hcaptchaCheck(window) {
        if (window.document.title.toLowerCase().includes('cloudflare')) {
            return hcaptcha;
        } else {
            hcaptchaCounter = 0;
        }
    },
    function serviceUnavailableCheck(window) {
        if (window.document.querySelector('h2.error')) {
            const e = window.document.querySelector('h2.error');
            if (e.textContent.includes('unable to connect')) {
                return serviceUnavailable;
            }
        }
    },
    function changeLanguageCheck(window) {
        if (
            window.location.href.includes('vfsvisaservicesrussia.com/Global-Appointment')
            && window.document.querySelector(internal.languageSelector)
        ) {
            if (!window.document.querySelector(internal.languageSelector).selectedOptions[0].innerText.toLowerCase().includes(internal.targetLanguage.toLowerCase())) {
                return step0_changeLanguage;
            }
        }
    },
    function authCheck(window) {
        if (
            window.location.href.includes('vfsvisaservicesrussia.com/Global-Appointment')
            && window.document.querySelector(internal.zeroPageTitleSelector)?.innerText.toLowerCase().includes(internal.zeroPageTitle.toLowerCase())
        ) {
            return step0_authorize;
        }
    },
    function moveToFormCheck(window) {
        if (window.location.href.includes('vfsvisaservicesrussia.com/Global-Appointment/Home')) {
            if (
                window.document.querySelector(internal.loggedInSelector)?.innerText.toLowerCase().includes(internal.loggedInText.toLowerCase())
                && window.document.querySelector(internal.leftPanelSelector)?.textContent.trim()
                && !window.document.querySelector(internal.rightPanelSelector)?.textContent
            ) {
                return step0_moveToForm;
            } else if (
                window.document.querySelector(internal.loggedInSelector)?.innerText.toLowerCase().includes(internal.loggedInResidentText.toLowerCase())
                && !window.document.querySelector(internal.rightPanelSelector)?.textContent
            ) {
                // how bot got that page???
                return scheduleRestart;
            }
        }
    },
    function formInitialCheck(window) {
        if (
            window.location.href.includes('vfsvisaservicesrussia.com/Global-Appointment/Home/SelectVAC')
            && window.document.querySelector(internal.centreSelector)?.selectedIndex === 0
        ) {
            return step1;
        }
    },
    function formCheck2(window) {
        if (
            window.location.href.includes('vfsvisaservicesrussia.com/Global-Appointment/Home/SelectVAC')
            && window.document.querySelector(internal.centreSelector)?.selectedIndex !== 0
        ) {
            return step2;
        }
    },
    function step3Check(window) {
        if (window.document.querySelector('#ApplicantListForm')) {
            return step3;
        }
    }
];

/** @param {Window} window */
function getPageProcessor(window) {
    console.log('getPageProcessor');
    for (const check of checkers) {
        if (typeof check === 'function') {
            const temp = check(window);
            if (temp) return temp;
        }
    }
    return null;
}

async function main() {
    const nwwindow = await openURLAndGetNWWindow(url);
    await sleep(150);
    // await documentReady(nwwindow.window);
    log('Google recaptcha avoidness preparation');
    await prepareGoogleCaptchaAvoidness(nwwindow);
    await sleep(1000);
    nwwindow.window.location.href = url;
    await waitForChromiumEvent(nwwindow, 'loaded');
    let attention = false;
    log('startup');
    while (true) {
        console.log('loop');
        await sleep(500);
        if (state.restartRequired) {
            log('restarting');
            nwwindow.close();
            state.restartRequired = false;
            await sleep(200);
            // prevent possible callstack overflow using setTimeout
            setTimeout(main, 200);
            return;
        }
        if (state.enabled) {
            console.log('state enabled');
            const next = getPageProcessor(nwwindow.window);
            console.log(next);
            await sleep(500);
            try {
                await next(nwwindow.window, nwwindow);
                attention = false;
            } catch(e) {
                log(e);
                if (!next) {
                    log('current page URL:', nwwindow.window.location.href);
                    log('pausing');
                    state.enabled = false;
                }
                await sleep(5000);
            }
        } else if (!attention) {
            return;
        }
    }
}

main();
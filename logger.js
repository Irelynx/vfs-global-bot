const util = require('util');

/** @param {Document} document */
function initLog(document) {
  if (!document || !(document instanceof HTMLElement)) {
    if (module.exports.log) {
      // special case if .log already created
      module.exports.log.call(null, arguments);
    }
  }
  const opts = {
    scrollToLastOne: true
  };
  function log(...args) {
    const logEntry = document.createElement('pre');
    for (let i = 0; i < args.length; i++) {
      if (typeof args[i] === 'object') {
        logEntry.innerText += util.inspect(args[i]);
      } else {
        logEntry.innerText += args[i];
      }
      if (i + 1 !== args.length) {
        logEntry.innerText += ' ';
      }
    }
    if (log.element) {
      log.element.appendChild(logEntry);
      if (opts.scrollToLastOne) {
        logEntry.scrollIntoView();
      }
    } else {
      // throw new Error('Log Element is gone!');
      console.log(logEntry.innerText);
    }
  }

  function createOptions() {
    const div = document.createElement('div');
    const scrollToLastInput = document.createElement('input');
    scrollToLastInput.type = 'checkbox';
    scrollToLastInput.id = 'scrollToLastOneChkbox';
    scrollToLastInput.addEventListener('change', () => {
      opts.scrollToLastOne = !!scrollToLastInput.checked;
    });
    scrollToLastInput.checked = opts.scrollToLastOne;
    const label = document.createElement('label');
    label.for = 'scrollToLastOneChkbox';
    label.innerText = 'Scroll to last entry ';
    div.appendChild(label);
    div.appendChild(scrollToLastInput);
    div.className = 'controls';
    log.element.appendChild(div);
  }

  log.element = document.getElementById('console');
  createOptions();

  module.exports.log = log;
  return log;
}

module.exports = initLog;

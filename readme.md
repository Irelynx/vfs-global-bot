# VFS Finland Bot (based on NW.JS)

> NOTE: application under development

## Why?

This project used to automate appointment submission due to impossibility (kinda virtual) of submitting the form manually due to restrictions from VFS-Global side and due to unfair booking bots

> NOTE: This bot is working ONLY on English version of Appointment page, but can be easily adapted to other languages (bot will change the language of the page itself if it can)

## Start

1. Install dependencies
    1. Install [Node.js](https://nodejs.org)
    2. Install [NW.JS](https://nwjs.io)
        * ensure that `nw` is can be accessed from command line (add it to `PATH` environment variable, or change `start.bat` file)
    3. Install project dependencies:
        * run in current working directory in console (cmd/powershell/bash/...):
            ```bash
            npm install
            # add -D to the end to also install development dependencies
            ```
2. Edit configuration files
    * Copy `.env.sample` to `.env` and edit it. For fields description see [table below](#description-of-fields-in-env-file)
3. Start
    * Start via `start.bat` (just double click)
        * if you just unpack `nw` into folder, for example: `D:\Software\Tools\nwjs-sdk-v0.60.0-win-x64\`, then **EDIT** `start.bat` to start `nw` from that directory (see comments in `start.bat`)
    * Start via command line:
        ```bash
        nw .
        ```

## Description of fields in env file

> **NOTE**: Some examples in table below provided just as reference, they may change at any time in VFS interface!

| Field name | Type | Example | Description |
| - | - | - | - |
| `GA_EMAIL`                    | string  | `account@gmail.com`  | Your email address of Google Account to avoid Google ReCaptcha v2 |
| `GA_PASSWORD`                 | string  | `GooglePassword`     | Your password of Google Account to avoid Google ReCaptcha v2 (be aware of 2FA authorization or turn it off) |
| `GA_URL`                      | string  | `URL`                | SignIn URL to avoid Google ReCaptcha v2 |
| `APP_REFRESH_INTERVAL`        | number  | `1`                 | Page refresh interval in **minutes**. By default is `1` |
| `APP_ENABLE_SOUND_ATTENTION`  | boolean | `true`              | **DISABLED** Enable sound attention alongside with visual attention (window maximization and blinking) |
| `APP_SOUND_FILEPATH`          | string  | `./alarm-1.ogg`     | **DISABLED** Sound attention playback file path. Can handle `.mp3`, `.ogg`, `.wav`, `.mp4` (audio only), `.mkv` and other basic formats. |
||
| `APP_USER_EMAIL`              | string  | `account@domain`    | VFS manually registered account login |
| `APP_USER_PASSWORD`           | string  | `VFSPassword`       | VFS manually registered account password |
| `APP_CUSTOMER_EMAIL`          | string  | `account@domain`    | OPTIONAL User email, if not set `APP_USER_EMAIL` will be used instead |
| `APP_VISA_CENTER`             | string  | `petersburg`        | VFS centre name (First appointment page. One of the option text of `Centre`, case INSENSITIVE, but try to match original case) |
| `APP_VISA_CATEGORY`           | string  | `normal`            | VFS appointment category. (First appointment page. One of the option of `Appointment Category`, case INSENSITIVE) |
| `APP_FIRST_NAME`              | string  | `John`              | Your first name |
| `APP_LAST_NAME`               | string  | `Doe`               | Your last name |
| `APP_BIRTH_DATE`              | string  | `DD/MM/YYYY`        | Your birth date |
| `APP_PHONE`                   | string  | `9998887766`        | Your phone number |
| `APP_NATIONALITY`             | string  | `russian federation`   | Your nationality |
| `APP_PASSPORT_NUMBER`         | string  | `AABBBBBBB`         | Your passport number |
| `APP_PASSPORT_EXPIRE_DATE`    | string  | `DD/MM/YYYY`        | Your passport expiration date |

## How it works?

This bot written on JavaScript and uses NW.JS (aka Node-Webkit, based on Node.js and Chromium browser) to simulate user input (clicks, text inputs, ...).

Technically, NW.JS start only with one window, that used in this bot as the `Main` window. In the `Main` window, the `log` is displayed and the data of the `Target` window is processed.

Just right after the start, script opens new `Target` window on VFS LogIn URL, grabs its reference and simulate user actions within it.

Bot also trying to solve Captcha as best as it can.

## Used projects/packets:

* [NW.js](https://nwjs.io):
    * [Node.js](https://nodejs.org)
    * [Chromium](https://chromium.org/Home/)
* Node.js packets (from NPM):
    * [dotenv](https://npmjs.com/package/dotenv) - Store configuration in `.env` file
    * [@types/nw.js](https://npmjs.com/package/@types/nw.js) - for extending support (typings) for NW.JS in IDE


# Tasks/TODOs

* [ ] Captcha bypass
    * [x] Google ReCaptcha v2 simple bypass
        * [x] Authorization into Google Account
        * [x] Authorization checks
        * [x] (simple, often) click on the captcha "checkbox"
        * [ ] (hard, rare) resolving real captcha
            * currently bot just reload the whole window..
    * [x] hCaptcha (CloudFlare) simple bypass
        * [x] (simple, often) timeout
        * [ ] (simple, rare) button click (TODO)
            * currently bot just reload the while window..
        * [ ] (hard, rare) real captcha
            * currently bot just reload the while window..
    * [x] Graphics Captcha bypass
        * the application follows a special URL to bypass that captcha, but it CAN be recognized
* [x] Appointment Workflow
    * [x] Step 0:
        * [x] Language check/change
        * [x] Authorization
        * [x] Move to "Schedule Appointment" tab
    * [x] Step 1 - fill "Select Centre" initial form:
        * [x] Pick valid centre
        * [x] Try again if there are no open "seats" available
    * [x] Step 2 - fill Appointment category
        * [x] Pick valid appointment category
        * [x] Press "continue" button
            * [ ] (TODO) check for bugs
    * [x] Step 3 - (TODO) Fill the customers
        * [x] Press "add customer" button
        * [x] wait for next page is loaded
        * [x] fill new customer form data fields
        * [x] choose date and time
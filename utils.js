const crypto = require("crypto");
const fs = require("fs");
const stream = require("stream");
const { log } = require("./logger");

/**
 * Micro-task sleep for async/await
 * @param {number} time - time to sleep, in ms
 * @return {Promise}
 */
function sleep(time) {
  return new Promise((resolve) => {
    setTimeout(resolve, time);
  });
}

/**
 * Opens url in new window, and resolves with nwwindow
 * @param {string} url
 * @return {Promise<NWJS_Helpers.win>}
 */
function openURLAndGetNWWindow(url) {
  return new Promise((resolve) => {
    nw.Window.open(url, {}, (new_win) => {
      resolve(new_win);
    });
  });
}

/**
 * Resolves after DOMContentLoaded event
 * @param {Window} win
 * @return {Promise<void>}
 */
function documentReady(win) {
  return new Promise((resolve) => {
    if (!win.document || win.document.readyState === "loading") {
      win.addEventListener("load", () => {
        setTimeout(resolve, 0);
      });
    } else {
      resolve();
    }
  });
}

/**
 * Calculate hash of passed data
 * @param {Buffer | string | ReadableStream} data - passing data
 * @param {string} [algorithm='sha256'] - hashing algorithm
 * @return {Promise<string>}
 */
function hash(data, algorithm = "sha256") {
  return new Promise((resolve, reject) => {
    const hasher = crypto.createHash(algorithm);
    if (typeof data === "string" || data instanceof Buffer) {
      hasher.update(data);
      resolve(hasher.digest("hex"));
    } else if (data instanceof stream.Readable) {
      // data is stream now
      data.on("readable", () => {
        const _data = data.read();
        if (_data) {
          hasher.update(_data);
        } else {
          resolve(hasher.digest("hex"));
        }
      });
      data.on("error", reject);
    } else {
      reject(new Error("Unexpected data type"));
    }
  });
}

/**
 * Check filepath existance
 * @param {string} filepath - target to check
 */
function fileExists(filepath) {
  return new Promise((resolve) => {
    fs.access(filepath, fs.constants.R_OK, (error) => {
      resolve(!error);
    });
  });
}

/**
 * Wait for next tick
 */
function nextTick() {
  return new Promise((resolve) => {
    process.nextTick(resolve);
  });
}

/**
 * Returns IP address from headers like x-forwarded-for or remote socket IP
 * @param {http.IncomingMessage} request - HTTP request object
 * @return {string}
 */
function getIP(request) {
  // proxy: x-forwarded-for: client, proxy1, proxy2, ...
  return (
    (request.headers["x-forwarded-for"] || "").split(",").pop() ||
    // proxy: X-Real-IP
    request.headers["x-real-ip"] ||
    request.connection.remoteAddress ||
    request.socket.remoteAddress ||
    (request.connection.socket ? request.connection.socket.remoteAddress : null)
  );
}

/**
 * log in to Google account to avoid Google reCaptcha v2/v3. Average time to await: 10s
 * @param {NWJS_Helpers.win} nwwindow
 */
async function prepareGoogleCaptchaAvoidness(nwwindow) {
  const userInputTimeSkip = 100;
  nwwindow.window.location.href = process.env.GA_URL;
  await sleep(userInputTimeSkip * 4);
  await Promise.race([documentReady(nwwindow.window), sleep(5000)]);
  await sleep(500);

  if (nwwindow.window.document.querySelector('input[name="q"]')) {
    log("prepareGoogleCaptchaAvoidness - already logged in");
    return;
  }
  log("prepareGoogleCaptchaAvoidness - logging in");
  const email = nwwindow.window.document.querySelector('input[type="email"]');
  if (!email) {
    log("input[email] is not defined!");

    throw new Error("Can't login to Google account");
  }
  email.value = process.env.GA_EMAIL;
  email.dispatchEvent(new nwwindow.window.Event("change", { bubbles: true }));
  await sleep(userInputTimeSkip);
  nwwindow.window.document.querySelector("#identifierNext button").click();

  await sleep(500);
  while (
    !nwwindow.window.document.querySelector(
      'input[type="password"][name="password"]'
    )
  ) {
    // eslint-disable-next-line no-await-in-loop
    await sleep(500);
  }
  await sleep(500);

  const password = nwwindow.window.document.querySelector(
    'input[type="password"][name="password"]'
  );
  password.value = process.env.GA_PASSWORD;
  password.dispatchEvent(
    new nwwindow.window.Event("change", { bubbles: true })
  );
  await sleep(userInputTimeSkip);
  nwwindow.window.document.querySelector("#passwordNext button").click();
  await sleep(3000);
}

/**
 * @param {Window} window
 * @returns {Promise<NWJS_Helpers.win>}
 */
function getNwWindow(window) {
  return new Promise((resolve, reject) => {
    nw.Window.getAll((windows) => {
      for (const win of windows) {
        if (win.window === window) {
          resolve(win);
        }
      }
      // alternative
      for (const win of windows) {
        if (win.window.location.href === window.location.href) {
          resolve(win);
        }
      }
      reject(new Error("Can't find NWJS window"));
    });
  });
}

/**
* @param {Window} window
* @returns {Promise<void>}
*/
function waitForPageUnload(window) {
  return new Promise((resolve) => {
    window.addEventListener('unload', resolve);
  });
}

/**
* returns Google reCaptcha iframe from passed window, if founded
* @param {Window} win
* @return {HTMLIFrameElement|undefined}
*/
function getReCaptchaIFrame(win) {
  const qs = win.document.querySelectorAll('iframe');
  // usually, it is first iframe on the page..
  for (let i = 0; i < qs.length; i++) {
    if (qs[i].src.indexOf('recaptcha') > -1) {
      return qs[i];
    }
  }
  return undefined;
}

/**
 * @param {NWJS_Helpers.win} nwwindow
 * @param {'loaded' | 'close' | 'focus' | 'blur' | 'closed' | 'document-start' | 'document-end' | 'new-win-policy' | 'navigation' | 'move'} event
 * @returns {Promise<void|any>}
 */
function waitForChromiumEvent(nwwindow, event) {
  return new Promise((resolve) => {
    nwwindow.once(event, resolve);
  });
}

module.exports = {
  sleep,
  openURLAndGetNWWindow,
  documentReady,
  hash,
  fileExists,
  nextTick,
  getIP,
  prepareGoogleCaptchaAvoidness,
  getNwWindow,
  waitForPageUnload,
  getReCaptchaIFrame,
  waitForChromiumEvent,
};
